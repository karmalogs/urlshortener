from mock import MagicMock
from config import diconf
from server import app
from core.policy import Policy
import json


def _test_bad_url(url):
    with app.test_client() as client:
        res = client.post('/', data=dict(url=url))
        assert res.status_code == 400

        response_json = json.loads(res.data)
        assert response_json['message'] == 'Missing/Invalid url'


def test_bad_url():
    _test_bad_url(None)
    _test_bad_url('htt://a.com')


def test_guest_access():
    url = 'http://google.com'

    # replace db with test db id
    diconf.bind('dbConfig', {'host': 'redis', 'port': 6379, 'db': '0'}) \
            .bind('policyMapping', {'TEST_REG': Policy('TEST_REGISTERED', 100), None: Policy('GUEST', 1)})

    with app.test_client() as client:
        headers = {}
        headers[diconf.inject('apiKeyIdentifier')] = 'TEST'
        res = client.post('/', data=dict(url=url), headers=headers)

        assert res.status_code == 200
        response_json = json.loads(res.data)

        assert response_json['policy'] == 'GUEST'

    with app.test_client() as client:
        headers = {}
        headers[diconf.inject('apiKeyIdentifier')] = 'TEST_REG'
        res = client.post('/', data=dict(url=url), headers=headers)

        assert res.status_code == 200
        response_json = json.loads(res.data)

        assert response_json['policy'] == 'TEST_REGISTERED'


def test_lookup():
    db = diconf.inject('db')
    test_url = 'http://test.com'
    db.put('test', test_url, 10)

    with app.test_client() as client:
        res = client.get('/test')
        assert res.status_code == 301
        assert res.headers['Location'] == test_url

        res_noexist = client.get('/notfound')
        assert res_noexist.status_code == 404
