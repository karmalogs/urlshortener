from mock import MagicMock
from lib.redisdb import RedisConn


def test_db_interface():
    methods = dir(RedisConn)
    assert 'get' in methods
    assert 'expire' in methods
    assert 'has_key' in methods
    assert 'raw_connection' in methods

