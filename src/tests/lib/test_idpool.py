from mock import MagicMock
from lib.redisidpool import RedisIdPool


def test_allot():
    conn = MagicMock()
    incr = MagicMock()
    conn.raw_connection = MagicMock(return_value=incr)

    pool = RedisIdPool(conn, counterKey='test_counter')
    pool.allot(1)

    incr.incrby.assert_called_once_with('test_counter', 1)

