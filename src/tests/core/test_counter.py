from mock import MagicMock, call
from core.counter import CounterGen

import types


def _pool():
    sample_pool = MagicMock()
    sample_pool.allot = MagicMock(return_value = (0, 9))
    return sample_pool


def test_counter_gen():
    pool = _pool()
    obj = CounterGen(pool, pool_limit=10, __singleton=False)
    gen = obj.get()
    next(gen)
    assert isinstance(gen, types.GeneratorType)
    pool.allot.assert_called_once_with(10)


def test_counter_gen_reallot():
    pool = _pool()
    obj = CounterGen(pool, pool_limit=10, __singleton=False)
    gen = obj.get()
    [next(gen) for _ in range(11)]
    pool.allot.assert_has_calls([call(10), call(10)])

