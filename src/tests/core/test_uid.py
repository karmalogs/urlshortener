from mock import MagicMock, call, patch
from core.uid import UniqueIdGen
from core.counter import CounterGen


def _hasher():
    hasher = MagicMock()
    hasher.encode = MagicMock(return_value='test')
    return hasher


def _counter():
    obj = MagicMock()
    obj.get = MagicMock(return_value=iter(range(10)))
    return obj


def test_uid_init():
    hasher, counter = _hasher(), _counter()
    obj = UniqueIdGen(hasher, counter, __singleton=False)
    counter.get.assert_called_once()


def test_uid_get():
    hasher, counter = _hasher(), _counter()
    obj = UniqueIdGen(hasher, counter, __singleton=False)
    encoded_val = next(obj.get())
    hasher.encode.assert_called_once_with(0)
    assert encoded_val == 'test'

