from mock import Mock
from core.policy import Policy, PolicyStore


def test_policy():
    mock_mapping = {'TEST_KEY': Policy('Test', 100), None: Policy('Guest', 0)}
    store = PolicyStore(mock_mapping, __singleton=False)

    assert store.get_policy('TEST_KEY') == mock_mapping['TEST_KEY']
    assert store.get_policy('INVALID') == mock_mapping[None]

