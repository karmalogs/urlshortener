from core.policy import Policy


REGISTERED_POLICY = Policy('REGISTERED', 30 * 24 * 60 * 60) # 30 days retention period
GUEST_POLICY = Policy('GUEST', 24 * 60 * 60) # 1 day retention period

