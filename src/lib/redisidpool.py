
class RedisIdPool(object):

    def __init__(self, conn, counterKey='counter'):
        self.conn = conn.raw_connection()
        self.counter_key = counterKey

    def allot(self, size):
        new_index = self.conn.incrby(self.counter_key, size)
        return (new_index - size, new_index)

