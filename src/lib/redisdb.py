from redis import StrictRedis


class RedisConn(object):
    def __init__(self, dbConfig):
        db, host, port = dbConfig['db'], dbConfig['host'], dbConfig['port']
        self.conn = StrictRedis(host=host, port=port, db=db)

    def raw_connection(self):
        return self.conn

    def expire(self, key, val, secs):
        self.conn.setex(key, secs, val)

    def get(self, key):
        val = self.conn.get(key)
        return val.decode('utf-8') if val else None

    def has_key(self, key):
        return self.conn.exists(key)

