from .errors import CollissionException
from lib.patterns.singleton import SingletonPatternMetaclass


class Db(object):

    __metaclass__ = SingletonPatternMetaclass

    def __init__(self, conn):
        self.conn = conn

    def put(self, key, val, expire_time_secs):
        conn = self.conn
        if conn.has_key(key):
            raise CollissionException(key)
        conn.expire(key, val, expire_time_secs)

    def get(self, key):
        return self.conn.get(key)

