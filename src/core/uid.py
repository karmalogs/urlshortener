from lib.patterns.singleton import SingletonPatternMetaclass


class UniqueIdGen(object):

    __metaclass__ = SingletonPatternMetaclass

    def __init__(self, hasher, counterGen):
        self.hasher = hasher
        self.counter = counterGen.get()

    def get(self):
        counter = self.counter
        while True:
            yield self.hasher.encode(next(counter))

