from lib.patterns.singleton import SingletonPatternMetaclass


class Policy(object):

    def __init__(self, name, ttl):
        self.name = name
        self.ttl = ttl


class PolicyStore(object):

    __metaclass__ = SingletonPatternMetaclass

    def __init__(self, policyMapping):
        self.policy_mapping = policyMapping

    def get_policy(self, apiKey):
        return self.policy_mapping.get(apiKey) or self.policy_mapping.get(None)

