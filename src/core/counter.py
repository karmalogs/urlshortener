from lib.patterns.singleton import SingletonPatternMetaclass


class CounterGen(object):

    __metaclass__ = SingletonPatternMetaclass

    def __init__(self, counterPool, pool_limit=100):
        self.pool = counterPool
        self.pool_limit = pool_limit

    def get(self):
        start_index, end_index = (0, 0)
        while True:
            if start_index == end_index:
                start_index, end_index = self.pool.allot(self.pool_limit)

            yield start_index
            start_index += 1

