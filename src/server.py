from flask import Flask, request, jsonify, url_for, redirect

from config import diconf
from utils.urlvalidator import is_valid_url

import sys
import os


app = Flask(__name__)
app.debug = os.environ.get('ENVIRONMENT', 'production') == 'development'


@app.route('/', methods=['POST'])
def index():
    url = request.form.get('url', None)

    if not url or not is_valid_url(url):
        message = jsonify({'message': 'Missing/Invalid url'})
        return message, 400

    api_key = request.headers.get(diconf.inject('apiKeyIdentifier'))

    policy = diconf.inject('policyStore').get_policy(api_key)

    uidGen = diconf.inject('uidGen').get()
    db = diconf.inject('db')

    key = next(uidGen)
    db.put(key, url, policy.ttl)

    return jsonify({'url': url_for('lookup', uid=key, _external=True),
                    'ttl': policy.ttl, 'policy': policy.name})


@app.route('/<uid>', methods=['GET'])
def lookup(uid):
    url = diconf.inject('db').get(uid)
    if url:
        return redirect(url, code=301)

    return '', 404


if __name__ == '__main__':
    app.run(port=int(sys.argv[1]))

