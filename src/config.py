from core.uid import UniqueIdGen
from core.db import Db
from core.counter import CounterGen
from core.policy import PolicyStore
from lib.redisdb import RedisConn
from lib.redisidpool import RedisIdPool
from lib.patterns.di import DIConfig
from hashids import Hashids

from policies import REGISTERED_POLICY, GUEST_POLICY

import logging

policyMapping = {'72MtAeA7S24JgF68q6zqj6TxGgST09UT': REGISTERED_POLICY, None: GUEST_POLICY}
dbConfig = dict(host='redis', port=6379, db='0')

diconf = DIConfig(logger=logging).bind('apiKeyIdentifier', 'X-API-KEY') \
            .bind('dbConfig', dbConfig).bind('conn', RedisConn).bind('db', Db) \
            .bind('counterGen', CounterGen).bind('uidGen', UniqueIdGen) \
            .bind('salt', 'Ci1NLS3V85').bind('hasher', Hashids) \
            .bind('counterPool', RedisIdPool).bind('pool_limit', 10).bind('counterKey', 'counter') \
            .bind('policyMapping', policyMapping).bind('policyStore', PolicyStore)

