# Summary
Build a scalable URL shortener web application in Python, using any framework, library, or persistence mechanisms you like. Share your code and documentation. Bonus: Deploy it where we can try it.


# Design
The design of the application has been very simple so far, for every url that requires shortening a unique short id is created against it and that mapping is stored in Redisdb.
When the shortened url is pasted into the browser and the request hits the application it looks up the url from redis agaist the key and sends a 301 redirect.


## Specifics
### Unique Id generator
The unique id generator is based of on the concept of the library http://hashids.org/. We use this library to convert a number into a unique string.
The integer is unique amongst the cluster, there is no duplication of the same number irrespective of the number of instances of the app server, we achieve this by storing
the current counter in redis, please find below the pseudo code which help explains

```
  pool_size = 10
  redis.incrby counter_key, pool_size
  unique_ids_in_memory = [x, x+1, x+2, x+3,.....x+pool_size]; where x = redis.get(counter_key) - pool_size
```
Everytime an app server boots up it takes up a pool of numbers from the counter; once the pool is exhausted another request is made to the pool alloter for a new range

* Drawbacks:
  * Let's assume an app server boots up and requests the pool of size 20
  * App server consumed 12 of 20 and the server dies
  * A pool of 8 slots are not used and are wasted as the current count in the counter might have moved forward by now. There are ways of re-using the wasted pools but currently not
considered for this application

### API Profiles
The api profile to the application has two modes
* Guest
* Registered user

The api calls of a registered caller should have their api key mentioned in the header 'X-API-KEY', if valid the api will be given a registered user privilege else a guest privilege

### Privileges
I have defined privileges as `policies` in the code, and the only differentiating factor I have chosen so far is the TTL of the shortened link.
There are 2 policies defined so far
* GUEST: TTL of link = 1 day
* REGISTERED: TTL of link = 30 days

# Getting started
I've configured docker to get started of easily with the whole of environment. Please follow below steps to get the environment running.
`docker-compose build` &
`docker-compose up -d`

Once the commands are successfully executed, you can try api with the command:
`curl -H 'Accept: application/json' -d 'url=http://www.google.com' -XPOST http://localhost/` -v

This should return with a success response code of `200`.

# Deployment
A light-weight working version of this prototype is deployed on AWS EC2 @ http://184.73.10.62/.

The deployment is a simple micro EC2 instance with all docker containers running on the same host, without a front facing LB at the moment.

# Dependencies
Have relied on a couple of libraries and code blocks to help with the development
* Hashids: Library for generating unique id's from numbers
* Python-Patternlib: A collection of some of my most used programming patterns (https://github.com/kailashnath/python-patternlib)
* Copied a block of code w.r.t url validation that can be found in (src/utils/urlvalidator.py). Ignored to use a library for this purpose, as the only requirement was url validation rather than 


# My thoughts

## Scalability
* The application code is designed to be horizontally scalable due the approach we have chosen for `unique id` generation, however the probability of hitting the integer limit of `9223372036854775807` is almost remote giving us the opportunity to improve the optimization further.
* Currently I'm using a single redis db for storing all the keys, however this can be improved by deploying a redis cluster

## Availability
* It would be good to split the service into 2 parts. One which serves the purpose of url shortening and the other which helps with redirect, this would help us keep the application available irrespective of the loads against different api's
* Breaking the service as above also helps us scale one api call more aggressively than the other that can help with both scaling and availability

## Deployment automation
* Though docker eases the setup of environment locally, setting up an environment in a distributed production environment comes with it's own challenges of configuration management
* I would like to use Ansible for automation of provisioning and `etc.d` with `conf.d` for configuration management or `consul` 
* Would have ansible playbooks to setup the cloud automation

## Security
* Would resort to api gateways to throttle and manage the api quotas
* Would deploy the application in a VPC environment
* Would rely heavily on AWS load balancers to mitigate the DDOS attacks and keep the application from getting beaten up

## Coding Style
* Personally prefer functional programming
* Reduce mutability if relying heavily on classes and objects
* Try to follow design patterns
* Code review a MUST
* Test cases covering the core logic of the application to make the build secure but not for coverage reports
* Love dependency injection

## Testing
* Prefer TDD for development and BDD for functional, usually ends up with mix of both worlds
* Break the code into as many small functions as possible easing the process of writing tests without resorting to mocking too much
* In the current application, have written just enough test cases which cover the critical parts of the application
* Believes in interface based programming, where interfaces are first defined before the logic; this helps keep the design sane

## Internal doc / User doc
* As this application is pretty much developer focused, having some sort of automated documentation generator tool would help

## Performance
* As this application is heavily I/O bound, I would probably re-write this in Python3 with asyc I/O support or in NodeJS totally
* As the application is horizontally scalable performance should be easy to bump up
* At heavy traffic using something like Hazelcast will help boost the stability of the application without impacting much of the performance

